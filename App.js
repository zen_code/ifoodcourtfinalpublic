// import React, { Component } from 'react';
// import {
//   AppRegistry,
//   Text,
//   View,
//   TouchableHighlight,
//   NativeAppEventEmitter,
//   Platform,
//   PermissionsAndroid
// } from 'react-native';
// import BleManager from 'react-native-ble-manager';

// class App extends Component {

//     constructor(){
//         super()

//         this.state = {
//             ble:null,
//             scanning:false,
//         }
//     }

//     componentDidMount() {
//         BleManager.start({showAlert: false});
//         this.handleDiscoverPeripheral = this.handleDiscoverPeripheral.bind(this);

//         NativeAppEventEmitter
//             .addListener('BleManagerDiscoverPeripheral', this.handleDiscoverPeripheral );

//         if (Platform.OS === 'android' && Platform.Version >= 23) {
//             PermissionsAndroid.checkPermission(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
//                 if (result) {
//                   console.log("Permission is OK");
//                 } else {
//                   PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
//                     if (result) {
//                       console.log("User accept");
//                     } else {
//                       console.log("User refuse");
//                     }
//                   });
//                 }
//           });
//         }
//     }

//     handleScan() {
//         BleManager.scan([], 30, true)
//             .then((results) => {console.log('Scanning...'); });
//     }

//     toggleScanning(bool){
//         if (bool) {
//             this.setState({scanning:true})
//             this.scanning = setInterval( ()=> this.handleScan(), 3000);
//         } else{
//             this.setState({scanning:false, ble: null})
//             clearInterval(this.scanning);
//         }
//     }

//     handleDiscoverPeripheral(data){
//         console.log('Got ble data', data);
//         alert(JSON.stringify(data))
//         this.setState({ ble: data })
//     }

//     render() {

//         const container = {
//             flex: 1,
//             justifyContent: 'center',
//             alignItems: 'center',
//             backgroundColor: '#F5FCFF',
//         }

//         const bleList = this.state.ble
//             ? <Text> Device found: {this.state.ble.name} </Text>
//             : <Text>no devices nearby</Text>

//         return (
//             <View style={container}>

//                 {/* <Button onPress={() => this.toggleScanning(!this.state.scanning)} title="Start scanning"/> */}

//                 <TouchableHighlight style={{padding:20, backgroundColor:'#ccc'}} onPress={() => this.toggleScanning(!this.state.scanning) }>
//                     <Text>Scan Bluetooth ({this.state.scanning ? 'on' : 'off'})</Text>
//                 </TouchableHighlight>

//                 {bleList}
//             </View>
//         );
//     }
// }

// export default App;



import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  FlatList,
  AsyncStorage,
  ActivityIndicator,
  Platform,
} from "react-native";
import {
  createStackNavigator,
  createDrawerNavigator,
  createAppContainer,
  NavigationActions,
  NavigationAction
} from "react-navigation";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Text,
  Button,
  Left,
  Right,
  Body,
  Item,
  Card,
  CardItem,
  Input,
  Thumbnail,
  Fab,
  Icon
} from "native-base";

import SplashScreen from "./src/app/screen/SplashScreen";
import Menu from "./src/app/screen/Menu";
import Login from "./src/app/screen/Login";
import Register from "./src/app/screen/Register";
import MainMenu from "./src/app/screen/MainMenu";
import DetailStand from "./src/app/screen/DetailStand";
import Order from "./src/app/screen/Order";
import Bank from "./src/app/screen/Bank";
import TopUp from "./src/app/screen/TopUp";

const NavStack = createStackNavigator({
  SplashScreen: {
    screen: SplashScreen
  },
  Menu: {
    screen: Menu
  },
  Login: {
    screen: Login
  },
  Register: {
    screen: Register
  },
  MainMenu: {
    screen: MainMenu
  },
  DetailStand: {
    screen: DetailStand
  },
  Order: {
    screen: Order
  },
  Bank: {
    screen: Bank
  },
  TopUp: {
    screen: TopUp
  },

});

const App = createAppContainer(NavStack);

export default App;


// import React, {Component} from 'react';
// import {View,StatusBar} from 'react-native';
// import NavigationExperimental  from 'react-native-deprecated-custom-components';
// import Home from "./components/pages/home";

// export default class App extends Component {
//     _navigator = null;
//     constructor(props) {
//         super(props);
//     }
//     componentDidMount() {

//     }

//     renderScene(route, navigator) {
//         this._navigator = navigator;
//         let Component = route.component;
//         return <View style={{flex:1}}><StatusBar backgroundColor="lightblue" /><Component route={route} navigator={navigator} {...route.passProps}/></View>
//     }

//     render() {
//         return (
//             <NavigationExperimental.Navigator
//                 style={{flex: 1}}
//                 initialRoute={{component: Home}}
//                 configureScene={() => { return NavigationExperimental.Navigator.SceneConfigs.FloatFromRight; }}
//                 renderScene={this.renderScene}
//             />
//         );
//     }
// }