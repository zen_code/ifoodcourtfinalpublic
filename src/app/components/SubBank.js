import React, { Component } from "react";
import { View, Text, Image, TouchableNativeFeedback } from "react-native";
import { Icon } from "native-base";
import colors from "../../styles/colors";
import PropTypes from "prop-types";

let styles = {
  container: {
    flex: 1,
    height: '100%',
    width: '100%',
    marginLeft: 0,
    marginRight: 5,
    marginBottom: 10,
    borderRadius:10,
    marginTop:10,
    backgroundColor: colors.white,
  },
  b: {
    backgroundColor: colors.primary
  }
};

export default class SubBank extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { loading, disabled, handleOnPress } = this.props;
    return (
      <View style={[styles.container]}>
      <View style={{flexDirection:'row', marginBottom:5, marginTop:5}}>
        <View style={{width:'25%', justifyContent: "center", paddingLeft:5}}>
          <View style={{width:70, height:70, borderRadius:40}}>
   
          <Image
                source={this.props.imageUri}
                style={{ marginTop:0, width: 70, height: 70, resizeMode: "contain" }}
              />
          </View>
        </View>
        <View style={{width:'75%'}}>
          <Text style={{fontSize:18, color:colors.black, fontWeight:'bold'}}>{this.props.namaBank}</Text>
          <Text style={{fontSize:15, paddingTop:0}}>{this.props.noRekening}</Text>
          <Text style={{fontSize:15, paddingTop:0}}>{this.props.namaNasabah}</Text>
        </View>
      </View>
      </View>
    );
  }
}


SubBank.propTypes = {
  handleOnPress: PropTypes.func,
  disabled: PropTypes.bool
};
