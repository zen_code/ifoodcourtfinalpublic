import React, { Component } from "react";
import { View, Text, Image, TouchableNativeFeedback } from "react-native";
import { Icon } from "native-base";
import colors from "../../styles/colors";
import PropTypes from "prop-types";

let styles = {
  container: {
    flex: 1,
    height: '100%',
    width: '100%',
    marginLeft: 0,
    marginRight: 5,
    marginBottom: 10,
    borderRadius:10,
    marginTop:10,
    backgroundColor: colors.white,
  },
  b: {
    backgroundColor: colors.primary
  }
};

export default class SubStand extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { loading, disabled, handleOnPress } = this.props;
    return (
      <View style={[styles.container]}>
      <View style={{flexDirection:'row', marginBottom:10, marginTop:5}}>
        <View style={{width:'25%', justifyContent: "center", paddingLeft:5}}>
          <View style={{width:70, height:70, borderRadius:40}}>
            <Image
              source={this.props.imageUri}
              style={{ width: '100%', height: '100%', marginTop: 0, marginBottom: 2, borderRadius:10,}}
            />
          </View>
        </View>
        <View style={{width:'70%'}}>
          <Text style={{fontSize:18, color:colors.black, fontWeight:'bold'}}>{this.props.namaToko}</Text>
          <View style={{width:'20%'}}>
        <View style={{flexDirection:'row', flex:1, paddingTop:4}}>
          <View style={{paddingRight:2}}>
            <Icon
                name='ios-star'
                style={{fontSize:10, color:'#EB9201'}}
            />
          </View>
          <View style={{paddingRight:2}}>
            <Icon
                name='ios-star'
                style={{fontSize:10, color:'#EB9201'}}
            />
          </View>
          <View style={{paddingRight:2}}>
            <Icon
                name='ios-star'
                style={{fontSize:10, color:'#EB9201'}}
            />
          </View>
          <View style={{paddingRight:2}}>
            <Icon
                name='ios-star'
                style={{fontSize:10, color:'#EB9201'}}
            />
          </View>
          <View style={{paddingRight:2}}>
            <Icon
                name='ios-star'
                style={{fontSize:10, color:'#DEDFDF'}}
            />
          </View>
        </View>
        </View>
          <View style={{flex:1, flexDirection:'row'}}>
          <Text style={{fontSize:13, paddingTop:0}}>{this.props.caption}</Text>
          </View>
        </View>
      </View>
      </View>
    );
  }
}


SubStand.propTypes = {
  handleOnPress: PropTypes.func,
  disabled: PropTypes.bool
};
