import React, { Component } from 'react';
import {
  AppRegistry,
  Text,
  View,
  TouchableHighlight,
  NativeAppEventEmitter,
  Platform,
  PermissionsAndroid,
  AsyncStorage,
  Alert,
  ScrollView,
  FlatList,
  TouchableOpacity,
  RefreshControl,
  ActivityIndicator
} from 'react-native';

import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Body,
    Icon,
    Thumbnail
 } from "native-base";
import styles from "./styles/AllStand";
import BleManager from 'react-native-ble-manager';
import GlobalConfig from "../components/GlobalConfig";
import SubStand from "../components/SubStand";
import colors from "../../styles/colors";

var that;
class ListItem extends React.PureComponent {
  navigateToScreen(route, dataStand) {
    AsyncStorage.setItem("dataStand", JSON.stringify(dataStand)).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View style={{width:'100%', marginRight:0, borderBottomWidth:1, borderBottomColor:'#DEDFDF', marginLeft:15}}>
      <TouchableOpacity
        transparent
        onPress={() => this.navigateToScreen("DetailStand", this.props.data)}>
        <SubStand
            imageUri={{ uri: GlobalConfig.IMAGEHOST + this.props.data.image }}
            namaToko={this.props.data.nama_toko}
            caption={this.props.data.caption}
          />
        </TouchableOpacity>
      </View>
    );
  }
  }

export default class MainMenu extends Component {

    constructor(){
        super()
        this.state = {
            ble:null,
            scanning:false,
            token:'',
            listStand:[],
        }
    }

    static navigationOptions = {
        header: null
    };

    onRefresh() {
        console.log("refreshing");
        this.setState({ isLoading: true }, function() {
            this.loadStand();
        });
      }

    componentDidMount() {
        AsyncStorage.getItem("token").then(token => {
            this.setState({
              token: token,
              isLoading: false,
            });
            this.loadStand();
        });
        
        // BleManager.start({showAlert: false});
        // this.handleDiscoverPeripheral = this.handleDiscoverPeripheral.bind(this);

        // NativeAppEventEmitter
        //     .addListener('BleManagerDiscoverPeripheral', this.handleDiscoverPeripheral );

        // if (Platform.OS === 'android' && Platform.Version >= 23) {
        //     PermissionsAndroid.checkPermission(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
        //         if (result) {
        //           console.log("Permission is OK");
        //         } else {
        //           PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
        //             if (result) {
        //               console.log("User accept");
        //             } else {
        //               console.log("User refuse");
        //             }
        //           });
        //         }
        //   });
        // }
    }

    loadStand(){
        this.setState({
            isLoading: true,
        })
        var token_Authorization = 'Bearer ' + JSON.parse(this.state.token);
 
        var url = GlobalConfig.SERVERHOST + 'show';
        var formData = new FormData();
        formData.append("mac_address", 'AC:23:3F:26:08:F2,AC:23:3F:26:08:F1')

        fetch(url, {
            headers: {
            'Content-Type': 'multipart/form-data',
            'Authorization': token_Authorization,
            },
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    listStand:responseJson,
                    isLoading: false,
                })
            })
            .catch((error) => {
            Alert.alert('Tidak Ada Internet', 'Cek Koneksi Internet Anda', [{
                text: 'Ok'
            }])
            console.log(error)
            })
    }

    // handleScan() {
    //     BleManager.scan([], 30, true)
    //         .then((results) => {console.log('Scanning...'); });
    // }

    // toggleScanning(bool){
    //     if (bool) {
    //         this.setState({scanning:true})
    //         this.scanning = setInterval( ()=> this.handleScan(), 3000);
    //     } else{
    //         this.setState({scanning:false, ble: null})
    //         clearInterval(this.scanning);
    //     }
    // }

    // handleDiscoverPeripheral(data){
    //     console.log('Got ble data', data);
    //     this.setState({ ble: data })
    // }

    _renderItem = ({ item }) => <ListItem data={item} />;

    render() {

        // const container = {
        //     flex: 1,
        //     justifyContent: 'center',
        //     alignItems: 'center',
        //     backgroundColor: '#F5FCFF',
        // }

        // const bleList = this.state.ble
        //     ? <Text> Device found: {this.state.ble.name} </Text>
        //     : <Text>no devices nearby</Text>

        that = this;
        var list;
        if (this.state.isLoading) {
          list = (
            <View
              style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
            >
              <ActivityIndicator size="large" color="#330066" animating />
            </View>
          );
        } else {
          if (this.state.listStand == '') {
            list = (
              <View
                style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
              >
                <Thumbnail
                  square
                  large
                  source={require("../../assets/images/empty.png")}
                />
                <Text>No Data!</Text>
              </View>
            );
          } else {
            list = (
              <FlatList
                data={this.state.listStand}
                renderItem={this._renderItem}
                keyExtractor={(item, index) => index.toString()}
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.isLoading}
                    onRefresh={this.onRefresh.bind(this)}
                  />
                }
              />
            );
          }
        }

        return (
            <Container style={styles.wrapper}>
            <Header style={styles.header}>
              <Left style={{ flex: 1 }}>
                {/* <Button
                  transparent
                  onPress={() => this.props.navigation.navigate("Login")}
                >
                  <Icon
                    name="ios-arrow-back"
                    size={20}
                    style={styles.facebookButtonIconOrder2}
                  />
                </Button> */}
              </Left>
              <Body style={{ flex: 3, alignItems: "center" }}>
                <Title style={styles.textbody}>I Food Court</Title>
              </Body>
              <Right style={{ flex: 1 }}>
                {/* <Button transparent onPress={() => this.onClickSearch()}>
                  <Icon
                    name="ios-search"
                    size={20}
                    style={styles.facebookButtonIconOrder2}
                  />
                </Button> */}
              </Right>
            </Header>
            <View style={{ marginTop: 5, marginRight:20}}>
              {list}
            </View>
            </Container>


            // <View style={container}>

            //     {/* <Button onPress={() => this.toggleScanning(!this.state.scanning)} title="Start scanning"/> */}

            //     <TouchableHighlight style={{padding:20, backgroundColor:'#ccc'}} onPress={() => this.toggleScanning(!this.state.scanning) }>
            //         <Text>Scan Bluetooth ({this.state.scanning ? 'on' : 'off'})</Text>
            //     </TouchableHighlight>

            //     {bleList}
            // </View>
        );
    }
}
