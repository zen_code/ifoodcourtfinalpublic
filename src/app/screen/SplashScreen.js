import React from 'react';
import {
  View,
  Text,
  Image,
  Platform,
  Animated,
  Easing,
  ScrollView
} from 'react-native';
import {
  Container,
  Header,
  Left,
  Right,
  Button,
  Body,
  Title,
  Icon,
} from 'native-base'

import styles from "./styles/SplashScreen";
import colors from "../../styles/colors";

class SplashScreen extends React.Component {
  constructor(props) {
    super(props);
    this.animatedImage = new Animated.Value(0.2);
    this.animatedText = new Animated.Value(0);
  }

  static navigationOptions = {
    header: null
  };

  performTimeConsumingTask = async() => {
    return new Promise((resolve) =>
      setTimeout(
        () => { resolve('result') },
        2000
        // 200
      )
    )
  }

  async componentDidMount() {
    Animated.spring(this.animatedImage, {
      toValue: 1,
      friction: 4,
      delay: 200,
      duration: 8000,
      useNativeDriver: true,
    }).start();

    const data = await this.performTimeConsumingTask();

    if (data !== null) {
      this.props.navigation.navigate('Menu');
    }
  }

  render() {
    const imageStyle = {
        transform: [{ scale: this.animatedImage }]
    };

    return (
      <Container style={styles.wrapper}>
            <Animated.View style={[styles.ring, imageStyle]}>
                <Animated.Image
                  source={require('../../assets/images/logo.png')}
                  style={[
                      {
                        resizeMode: "contain",
                        width: 150,
                        height: 120
                      }
                  ]}
                />
            </Animated.View>
      </Container>
    );
  }
}


export default SplashScreen;
